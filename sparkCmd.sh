#!/bin/bash

# Load the hadoop fs and included commands
module load hadoop

# We could submit a spark file as a job (to the cluster)
spark-submit --master=yarn --num-executors 15 --driver-memory 8G --executor-memory 8G ${pyfile}

# This opens pyspark within python
pyspark --master=yarn --num-executors 25 --driver-memory 25g --executor-memory 25G
